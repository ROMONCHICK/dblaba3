# Лабараторная работа 3

### Создание таблицы Sales

```sql
-- Создание таблицы Sales
CREATE TABLE Sales (
    id SERIAL PRIMARY KEY,
    country VARCHAR(100),
    city VARCHAR(100),
    shop_id INT UNIQUE,
    date DATE,
    summa NUMERIC(10, 2)
);
```
### Заполнение таблицы Sales случайными данными
Для заполнения таблицы случайными данными мы используем скрипт на Python, который сгенерирует необходимые SQL-запросы для вставки данных. Запросы будут сохранены в файл insert_sales.sql.
```py
import psycopg2
import random
from random import randrange
from datetime import datetime
from datetime import timedelta

def random_date(start, end):
    delta = end - start
    int_delta = (delta.days * 24 * 60 * 60) + delta.seconds
    random_second = randrange(int_delta)
    return start + timedelta(seconds=random_second)

formate = '%Y/%m/%d'
d1 = datetime.strptime('2022/1/1', formate).date()
d2 = datetime.strptime('2022/12/31', formate).date()

connectionMarket = psycopg2.connect(
    dbname='Shabanov',
    user='postgres',
    password='Gjh302nmbGXZNla937mG39',
    host='localhost',
    port='5432'
)
cursorMarket = connectionMarket.cursor()
cursorMarket.execute("""
    CREATE TABLE IF NOT EXISTS Sales(
        ID SERIAL PRIMARY KEY,
        Country VARCHAR(50),
        City VARCHAR(50),
        Shop_ID INT,
        Date DATE,
        Summa INT
    );
""")
connectionMarket.commit()

SalesID = int(10000000)
CortSalesID = []

ShopID = 1

CountryNames = ["Russia", "UK", "USA", "Belorussia", "Spain", "China", "France", "Kazakhstan", "Turkey", "Germany"]
CortCountryNames = []

RussiaCityNames = ["Moskva", "SanktPetersburg", "Izhevsk", "Surgut", "Khanty-Mansisk", "Ekaterinburg", "Poltava", "Novgorod", "Barnaul", "Biysk", "Vladivostok", "Mozhaisk", "Sevastopol"]
UKCityNames = ["London", "York", "Manchester", "Yorkshire", "Oxford", "Birmingham", "Leeds", "Glasgow", "Sheffield", "Bradford", "Liverpool", "Edinburgh", "Cardiff", "Newcastle", "Coventry"]
USACityNames = ["Washington", "New York", "Los Angeles", "Chicago", "Boston", "San Francisco", "Miami"]
BelorussiaCityNames = ["Minsk", "Brest", "Grodno", "Vitebsk", "Gomel", "Mogilev", "Glubokoe", "Polotsk", "Lepel", "Pinsk", "Narovlya", "Krupki", "Turov", "Verkhnedvinsk", "Orsha", "Ivanovo", "Zhlobin"]
SpainCityNames = ["Madrid", "Seville", "Barcelona", "Malaga", "Granada", "Cordoba", "Valencia", "Santiago de Compostela"]
ChinaCityNames = ["Beijing", "Shanghai", "Harbin", "Guangzhou", "Chongqing", "Chengdu", "Xi'an", "Hangzhou", "Nanjing", "Wuhan", "Taiwan"]
FranceCityNames = ["Paris", "Nice", "Marseille", "Lyon", "Bordeaux", "Lille", "Montpellier", "Avignon", "La Rochelle"]
KazakhstanCityNames = ["Almaty", "Nur-Sultan", "Taraz", "Turkestan", "Shymkent", "Atyrau", "Aktau", "Rybachie"]
TurkeyCityNames = ["Antalya", "Istanbul", "Kemer", "Alanya", "Izmir", "Bodrum", "Side", "Edirne", "Ankara", "Adana"]
GermanyCityNames = ["Berlin", "Munich", "Hamburg", "Frankfurt", "Düsseldorf", "Cologne", "Dresden", "Nuremberg", "Hanover", "Bremen", "Trier", "Augsburg", "Mainz", "Aachen"]
CortCityNames = []
CortInput = []

# Создание словаря
CountryCitiesNames = {
    "Russia": RussiaCityNames,
    "UK": UKCityNames,
    "USA": USACityNames,
    "Belorussia": BelorussiaCityNames,
    "Spain": SpainCityNames,
    "China": ChinaCityNames,
    "France": FranceCityNames,
    "Kazakhstan": KazakhstanCityNames,
    "Turkey": TurkeyCityNames,
    "Germany": GermanyCityNames
}


while SalesID < 11000001:
    #CortSalesID.append(SalesID)
    CountryName = random.choice(CountryNames)
    #CortCountryNames.append(CountryNames[CountryID])
    CityName = random.choice(CountryCitiesNames.get(CountryName))
    a = random.randint(5, 10)
    for i in range(a):
        date = str(random_date(d1, d2))
        Summ = random.randint(10000, 1000000)
        cursorMarket.execute('INSERT INTO Sales (ID, Country, City, Shop_ID, Date, Summa) VALUES (%s, %s, %s, %s, %s, %s);', (SalesID, CountryName, CityName, ShopID, date, Summ))
        SalesID += 1
    ShopID += 1

connectionMarket.commit()
connectionMarket.close()
```

### Запросы для анализа данных
```sql
-- 1. Подсчитать количество строк в таблице
SELECT COUNT(*) AS total_rows FROM Sales;
```
```sql
-- 2. Подсчитать количество магазинов в каждой стране
SELECT country, COUNT(DISTINCT shop_id) AS total_shops FROM Sales GROUP BY country;
```
```sql
-- 3. Показать сумму продаж по магазинам
SELECT shop_id, SUM(summa) AS total_sales FROM Sales GROUP BY shop_id;
```
```sql
-- 4. Показать сумму продаж в определенной стране по магазинам за лето (июнь, июль, август)
SELECT shop_id, SUM(summa) AS summer_sales
FROM Sales
WHERE country = 'USA' AND date >= '2022-06-01' AND date <= '2022-08-31'
GROUP BY shop_id;
```

# Часть 2
### Создание копии таблицы Sales под наименованием Sales2

```sql
-- Создание копии таблицы Sales под наименованием Sales2
CREATE TABLE Sales2 AS TABLE Sales;
```
### Создание таблицы Shops

```sql
-- Создание таблицы Shops
CREATE TABLE Shops (
    shop_id INT PRIMARY KEY,
    city VARCHAR(100),
    country VARCHAR(100)
);
```
### Заполнение таблицы Shops уникальными данными

```sql
-- Заполнение таблицы Shops уникальными записями из таблицы Sales2
INSERT INTO Shops (shop_id, city, country)
SELECT DISTINCT shop_id, city, country
FROM Sales2;
```

### Удаление колонок City и Country из таблицы Sales2

```sql
-- Удаление колонок City и Country из таблицы Sales2
ALTER TABLE Sales2
DROP COLUMN city,
DROP COLUMN country;
```
## Ответы на вопросы с модифицированными запросами
### Показать сумму продаж в определенной стране по магазинам за лето

```sql
-- Показать сумму продаж в определенной стране (например, 'USA') по магазинам за лето
SELECT s.shop_id, SUM(s.summa) AS summer_sales
FROM Sales2 s
JOIN Shops sh ON s.shop_id = sh.shop_id
WHERE sh.country = 'USA' AND s.date BETWEEN '2022-06-01' AND '2022-08-31'
GROUP BY s.shop_id;
```

### Сумма продаж по городам

```sql
-- Показать сумму продаж по городам
SELECT sh.city, SUM(s.summa) AS total_sales
FROM Sales2 s
JOIN Shops sh ON s.shop_id = sh.shop_id
GROUP BY sh.city;
```

## Вычислить среднюю сумму продаж в разрезе
### Магазинов

```sql
-- Вычислить среднюю сумму продаж по магазинам
SELECT s.shop_id, AVG(s.summa) AS average_sales
FROM Sales2 s
GROUP BY s.shop_id;
```
### Городов

```sql
-- Вычислить среднюю сумму продаж по городам
SELECT sh.city, AVG(s.summa) AS average_sales
FROM Sales2 s
JOIN Shops sh ON s.shop_id = sh.shop_id
GROUP BY sh.city;
```

### Стран

```sql
-- Вычислить среднюю сумму продаж по странам
SELECT sh.country, AVG(s.summa) AS average_sales
FROM Sales2 s
JOIN Shops sh ON s.shop_id = sh.shop_id
GROUP BY sh.country;
```

## Создание SQL-функций
Давайте создадим запрашиваемые функции в PostgreSQL.

### Простая SQL-функция, возвращающая одну запись и два столбца

```sql
CREATE OR REPLACE FUNCTION get_single_record(input_value TEXT)
RETURNS TABLE (shop_id INTEGER, summa NUMERIC) AS $$
    SELECT s.shop_id, s.summa
    FROM Sales2 s
    JOIN Shops sh ON s.shop_id = sh.shop_id
    WHERE s.shop_id::TEXT = input_value OR sh.city = input_value OR sh.country = input_value
    LIMIT 1;
$$ LANGUAGE sql;
```

## Через OUT
```sql
CREATE OR REPLACE FUNCTION get_single_record(OUT shop_id INTEGER, OUT summa NUMERIC, input_value TEXT) AS $$
    SELECT DISTINCT ON (s.shop_id) s.shop_id, s.summa
    FROM Sales2 s
    JOIN Shops sh ON s.shop_id = sh.shop_id
    WHERE s.shop_id::TEXT = input_value OR sh.city = input_value OR sh.country = input_value;
$$ LANGUAGE sql;

```

### Простая SQL-функция, возвращающая несколько записей и два или более столбцов

```sql
CREATE OR REPLACE FUNCTION get_multiple_records(input_value TEXT)
RETURNS TABLE (shop_id INTEGER, summa NUMERIC, date DATE) AS $$
    SELECT s.shop_id, s.summa, s.date
    FROM Sales2 s
    JOIN Shops sh ON s.shop_id = sh.shop_id
    WHERE s.shop_id::TEXT = input_value OR sh.city = input_value OR sh.country = input_value;
$$ LANGUAGE sql;
```

### 8.3. Функция PL/pgSQL, возвращающая максимальную, минимальную и среднюю выручку

```sql
CREATE OR REPLACE FUNCTION get_sales_statistics(input_value TEXT)
RETURNS TABLE (description TEXT, value NUMERIC) AS $$
BEGIN
    RETURN QUERY
    SELECT 'Max Revenue' AS description, MAX(s.summa)::NUMERIC AS value
    FROM Sales2 s
    JOIN Shops sh ON s.shop_id = sh.shop_id
    WHERE s.shop_id::TEXT = input_value OR sh.city = input_value OR sh.country = input_value;
    
    RETURN QUERY
    SELECT 'Min Revenue' AS description, MIN(s.summa)::NUMERIC AS value
    FROM Sales2 s
    JOIN Shops sh ON s.shop_id = sh.shop_id
    WHERE s.shop_id::TEXT = input_value OR sh.city = input_value OR sh.country = input_value;
    
    RETURN QUERY
    SELECT 'Avg Revenue' AS description, AVG(s.summa)::NUMERIC AS value
    FROM Sales2 s
    JOIN Shops sh ON s.shop_id = sh.shop_id
    WHERE s.shop_id::TEXT = input_value OR sh.city = input_value OR sh.country = input_value;
END;
$$ LANGUAGE plpgsql;
```

### Пример использования

1. **Вызов функции `get_single_record`:**

```sql
SELECT * FROM get_single_record('1');  -- передаем shop_id в виде строки
```

2. **Вызов функции `get_multiple_records`:**

```sql
SELECT * FROM get_multiple_records('New York');  -- передаем город
```

3. **Вызов функции `get_sales_statistics`:**

```sql
SELECT * FROM get_sales_statistics('USA');  -- передаем страну
```